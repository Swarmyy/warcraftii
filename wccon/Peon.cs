using System;

namespace Peon
{
    public class Peon
    {
        public string race;
        public string faction;
        public int hp;
        public int armor;
        public Peon(string race, string faction, int hp, int armor)
        {
            this.race = race;
            this.faction = faction;
            this.hp = hp;
            this.armor = armor;
        }
        public string sayHello()
        {
            return string.Format("Bonjour");
        }
        public string grunt()
        {
            return string.Format("abu?");
        }
        public string talkToPeon(Peon peon)
        {

        }
    }
}