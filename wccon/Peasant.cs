using System;
namespace Peasant
{
    public class Peasant
    {
        public string race;
        public string faction;
        public int hp;
        public int armor;
        public Peon(string race, string faction, int hp, int armor)
        {
            this.race = race;
            this.faction = faction;
            this.hp = hp;
            this.armor = armor;
        }
        public string sayHello()
        {
            return string.Format("Bonjour");
        }
        public string talk()
        {
            return string.Format("Oui monseigneur?");
        }
        public string talkToPeasant(Peasant peasant)
        {
            return string.Format("Race: {1} \n Faction: {0} \n HP: {2} \n Armor: {3}")
        }
    }
}